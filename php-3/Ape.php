<?php 


class Ape extends Animal{
    protected $yell;
    
    public function __construct($name , $legs = 2, $cold_blooded = "no", $yell = "Auooo")
    {
        parent::__construct($name , $legs, $cold_blooded );
        $this->yell = $yell;
    }

    public function getYell(){
        return $this->yell;
    }

}



?>