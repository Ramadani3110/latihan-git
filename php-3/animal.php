<?php 
// release 0
class Animal{
    protected   $name,
                $legs,
                $cold_blooded;
    
    public function __construct($name , $legs = 4, $cold_blooded = "no")
    {
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
    public function getName(){
        return $this->name;
    }
    public function getLegs(){
        return $this->legs;
    }
    public function getCold_blooded(){
        return $this->cold_blooded;
    }

}    
// release 0




?>