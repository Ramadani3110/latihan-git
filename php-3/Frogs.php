<?php 


class Forgs extends Animal{
    protected $jump;
    
    public function __construct($name , $legs = 4, $cold_blooded = "no", $jump = "Hop Hop")
    {
        parent::__construct($name , $legs, $cold_blooded );
        $this->jump = $jump;
    }

    public function getJump(){
        return $this->jump;
    }

}



?>
