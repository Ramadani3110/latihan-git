<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome</title>
    <style>
        p{
            font-weight: bold;
            font-family: monospace;
            font-size: 2.1rem;
            text-transform: uppercase;
            display: inline-block;
            color: #0078AA;
            text-shadow: 3px 3px 0px #c2ced3, 4px 4px 0px rgb(0 0 0 / 10%);
        }
    </style>
  </head>
  <body>
    <h1>SELAMAT DATANG!</h1>
    <p>{{$namadepan}} {{$namabelakang}}</p>
    <h3>Terimakasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
  </body>
</html>