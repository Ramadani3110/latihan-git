<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register | Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
      <label for="first-name">First Name : </label> <br />
      <input type="text" name="first-name" id="first-name" /> <br />
      <br />
      <label for="last-name">Last Name : </label> <br />
      <input type="text" name="last-name" id="last-name" /> <br />
      <br />
      <label for="gender">Gender : </label> <br />
      <input type="radio" name="female" id="gender" />Female <br />
      <input type="radio" name="female" id="gender" />Male <br />
      <input type="radio" name="female" id="gender" />Other <br />
      <br />
      <label for="nationaly">Nationaly : </label> <br />
      <select name="nationaly" id="nationaly">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Singapura">Singapura</option>
        <option value="Australian">Australian</option>
      </select>
      <br />
      <br />
      <label for="language">Language spoken : </label> <br />
      <input type="checkbox" value="bindonesia" name="bindonesia" id="language" />Bahasa Indonesia <br />
      <input type="checkbox" value="binggris" name="binggris" id="language" />Enghlish <br />
      <input type="checkbox" value="other" name="other" id="language" />Other <br />
      <br />
      <label for="bio">Bio : </label> <br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br />
      <br />
      <input type="submit" name="submit" value="Sign Up" />
    </form>
  </body>
</html>
