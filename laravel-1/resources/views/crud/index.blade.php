@extends('adminlte.master')

@section('content')
<div class="card mx-1 my-2">
    <div class="card-header">
    <h3 class="card-title">Daftar Pemain Film</h3>
    </div>
        <div class="card-body">
            @if (session('berhasil'))
            <div class="alert alert-success">
                {{ session('berhasil') }}
            </div>
            @endif
            <a href="/cast/create" class="btn bg-gradient-info mb-3">Create Pemeran Baru</a>
            <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key => $pemain)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$pemain->nama}}</td>
                    <td align="center" style="display: flex; justify-content: center; align-items: center">
                        <a href="/cast/{{$pemain->id}}" class="btn btn-info">Lihat Detail</a>
                        <a href="/cast/{{$pemain->id}}/edit" class="btn btn-warning mx-1">Edit Data</a>
                        <form action="/cast/{{$pemain->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" align="center">No Data!!</td>
                </tr> 
                @endforelse
                
            </tbody>
            </table>
        </div>
    </div>
@endsection