@extends('adminlte.master')

@section('content')
<div class="card card-info mx-2 mt-3">
    <div class="card-header">
    <h3 class="card-title">Create Data Pemain Film Baru</h3>
    </div>
    <form class="form-horizontal" action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group row">
                <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
                @error('nama')
                    <div class="alert alert-danger py-1 px-1 text-sm mt-1">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="umur" class="col-sm-2 col-form-label">Umur</label>
                <div class="col-sm-10">
                <input type="number" name="umur" class="form-control" id="umur" placeholder="Umur">
                @error('umur')
                    <div class="alert alert-danger py-1 px-1 text-sm mt-1">{{ $message }}</div>
                @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="bio" class="col-sm-2 col-form-label">Bio</label>
                <div class="col-sm-10">
                <input type="text" name="bio" class="form-control" id="bio" placeholder="Bio">
                @error('bio')
                    <div class="alert alert-danger py-1 px-1 text-sm mt-1">{{ $message }}</div>
                @enderror
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info">Create</button>
            <button type="submit" class="btn btn-danger float-right">Batal</button>
        </div>
    </form>
</div>
@endsection