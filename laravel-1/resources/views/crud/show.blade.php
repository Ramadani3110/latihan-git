@extends('adminlte.master')

@section('content')
<div class="card mt-1 mx-3 p-3">
    <h3>Detail Data Pemeran</h3>
    <div class="card-body">
      <p>Nama : {{$cast->nama}}</p>
      <p>Umur : {{$cast->umur}}</p>
      <p>Bio : {{$cast->bio}}</p>
    </div>
  </div>
@endsection